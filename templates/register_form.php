<html>
<body style>
<div class="container">
<div id="middle">

    <!-- Fixed navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/login.php"><img src="img/circle2.png" alt="CS50 Study Buddy" />  CS50 Study Buddy</a>
        </div>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    
    <div class="container theme-showcase">
        <div class="page-header">
           <h1 id="tables"><br>Register</h1>
        </div>
    </div>
    
<form action="register.php" method="post">
    <fieldset>
        <div class="form-group">
            <input autofocus class="form-control" name="first" placeholder="First Name" type="text"/>
        </div>
        <div class="form-group">
            <input class="form-control" name="last" placeholder="Last Name" type="text"/>
        </div>
        <div class="form-group">
            <input class="form-control" name="year" placeholder="Class Year" type="text"/>
        </div>
        <div class="form-group">
            <input class="form-control" name="email" placeholder="Email" type="text"/>
        </div>
        <div class="form-group">
            <input class="form-control" name="emailconfirmation" placeholder="Confirm Email" type="text"/>
        </div>
        <div class="form-group">
            <input class="form-control" name="username" placeholder="Username" type="text"/>
        </div>
        <div class="form-group">
            <input class="form-control" name="password" placeholder="Password" type="password"/>
        </div>
        <div class="form-group">
            <input class="form-control" name="confirmation" placeholder="Confirm Password" type="password"/>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-default">Register</button>
        </div>
    </fieldset>
</form>
<div>
    or <a href="login.php">log in</a>
</div>
</body>
</html>
