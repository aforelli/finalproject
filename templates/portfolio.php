<html>
<link href="/css/bootstrap.css" rel="stylesheet" />

<body style>
<div class="container">
<div id="middle">

    <!-- Fixed navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php"><img src="img/circle2.png" alt="CS50 Study Buddy" />  CS50 Study Buddy</a>
        </div><div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Planner <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="classes.php">Add Courses</a></li>
                  <li><a href="schedule.php">Add Available Times</a></li>
                  <li><a href="appointment.php">Add Appointments</a></li>
                  <li class="divider"></li>
                  <li class="dropdown-header"></li>
                  <li><a href="myclasses.php">My Classes</a></li>
                  <li><a href="updatetimes.php">My Times</a></li>
                  <li><a href="calendar.php">My Calendar</a></li>
                </ul>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Buddy Up <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="findbuddy.php">Find Matches</a></li>
                  <li class="divider"></li>
                  <li class="dropdown-header"></li>
                  <li><a href="buddies.php">My Buddies</a></li>
                </ul>
              </li>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Account <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="settings.php">Settings</a></li>
                  <li class="divider"></li>
                  <li class="dropdown-header"></li>
                  <li><a href="logout.php">Logout</a></li>
                </ul>
              </li>
      </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    
    <div class="container theme-showcase">
        <div class="page-header">
           <h1 id="tables"><br>Welcome to</h1>
           <a href="/"><img alt="CS50 Study Buddy" src="/img/studybuddy.gif"/></a>
        </div>
    </div>
    
    <div class="container theme-showcase">

      <div class="well">
        <p>CS50 Study Buddy is a website created by students for students. Its purpose is to connect classmates with one another so that they can form study groups. 
        Study Buddy does this by linking you with your classmates based on a set of criteria, including availability, house or dorm, and year. 
        <br><br>
        To get started, add your classes and available times under the "Planner" tab, and then find buddies under the "Buddy Up" tab. Once you've 
        added these fields, you can start scheduling appointments with your buddies!
        </p>
      </div>


    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="../js/holder.js"></script>
  </body>
</html>

