<html>
<body style>
<div class="container">
<div id="middle">

    <!-- Fixed navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php"><img src="img/circle2.png" alt="CS50 Study Buddy" />  CS50 Study Buddy</a>
        </div><div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="dropdown active">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Planner <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="classes.php">Add Courses</a></li>
                  <li><a href="schedule.php">Add Available Times</a></li>
                  <li><a href="appointment.php">Add Appointments</a></li>
                  <li class="divider"></li>
                  <li class="dropdown-header"></li>
                  <li><a href="myclasses.php">My Classes</a></li>
                  <li><a href="updatetimes.php">My Times</a></li>
                  <li><a href="calendar.php">My Calendar</a></li>
                </ul>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Buddy Up <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="findbuddy.php">Find Matches</a></li>
                  <li class="divider"></li>
                  <li class="dropdown-header"></li>
                  <li><a href="buddies.php">My Buddies</a></li>
                </ul>
              </li>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Account <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="settings.php">Settings</a></li>
                  <li class="divider"></li>
                  <li class="dropdown-header"></li>
                  <li><a href="logout.php">Logout</a></li>
                </ul>
              </li>
      </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container theme-showcase">
        <div class="page-header">
           <h1 id="tables""><br>Add Appointment</h1>
        </div>
        <div class="">
        <p>To add an appointment, select a buddy and time. <br><br>
        Make sure you contact your buddy by email before submitting an appointment.
            </p>
      </div>
    </div> <!-- /container -->
    <form action="appointment.php" method="post">
    <fieldset>
    <div class="bs-example table-responsive">
              <table class="table table-striped table-bordered table-hover">
                <thead>
                  <tr>
                    <th >Select</th>
                    <th>First</th>
                    <th>Last</th>
                    <th>Year</th>
                    <th>Email</th>
                    <th>Select</th>
                    <th>Times</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                if(!empty($mybuds))
                {
                    for($i=0; $i<count($mybuds); $i++)
                    {
                        echo '<tr><td><input type="checkbox" class="form" name="checkbox[]" value="'. $mybuds[$i][0]['id'] . '"</td><td>';
                        print_r($mybuds[$i][0]['first'] . " ");
                        echo('</td><td>');
                        print_r($mybuds[$i][0]['last'] . " ");
                        echo('</td><td>');
                        print_r($mybuds[$i][0]['year'] . " ");
                        echo('</td><td>');
                        print_r($mybuds[$i][0]['email'] . " ");
                        
                        for ($j=0; $j<count($intersect[$i]); $j++)
                        {
                        echo '<td><input type="checkbox" class="form" name="time[]" value="'. $intersect[$i][$j]['time'] . '"</td><td>';
                        print_r($intersect[$i][$j]['time'] . " ");
                        echo('</td></tr><tr><td> </td><td> </td><td> </td><td> </td><td>');
                        
                        }
                        echo('<td></td><td></td></tr>');
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    <div class="form-group">
            <button type="submit" class="btn btn-default">Add Appointment</button>
        </div>
    </fieldset>
    </form>
        </div>
        
    </div>

<a href="javascript:history.go(-1);"></a>
</div>
</div>
<link href="../css/bootstrap.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="../js/holder.js"></script>
</body>
</html>
