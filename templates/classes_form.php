<html>
<body style>
<div class="container">
<div id="middle">

<!-- Fixed navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php"><img src="img/circle2.png" alt="CS50 Study Buddy" />  CS50 Study Buddy</a>
        </div><div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="dropdown active">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Planner <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="classes.php">Add Courses</a></li>
                  <li><a href="schedule.php">Add Available Times</a></li>
                  <li><a href="appointment.php">Add Appointments</a></li>
                  <li class="divider"></li>
                  <li class="dropdown-header"></li>
                  <li><a href="myclasses.php">My Classes</a></li>
                  <li><a href="updatetimes.php">My Times</a></li>
                  <li><a href="calendar.php">My Calendar</a></li>
                </ul>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Buddy Up <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="findbuddy.php">Find Matches</a></li>
                  <li class="divider"></li>
                  <li class="dropdown-header"></li>
                  <li><a href="buddies.php">My Buddies</a></li>
                </ul>
              </li>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Account <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="settings.php">Settings</a></li>
                  <li class="divider"></li>
                  <li class="dropdown-header"></li>
                  <li><a href="logout.php">Logout</a></li>
                </ul>
              </li>
      </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    <form action="classes.php" method="post">
    <fieldset>
        <div class="container theme-showcase">
            <div class="page-header">
               <h1 id="tables"><br>Find Classes</h1>
            </div>
        </div>
    

        
            <div class="form-group">
                <input autofocus class="form-control" name="coursename" placeholder="Coursename" type="text"/>
            </div>
            <div>
                <button type="submit" class="btn btn-default">Search</button>
            </div>
            
                
    

</fieldset>
</form>
<div>


    <?php
    if (empty($results)==FALSE)
    {?>
    <div class="container theme-showcase">
            <div class="page-header">
               <h1 id="tables"><br>Results</h1>
            </div>
        </div>
    <form action="myclasses.php" method="post">
<fieldset>
<div class="form-group">
    <button type="submit" class="btn btn-default">Add Checked Classes</button>
</div> 
    <div class="bs-example table-responsive">
              <table class="table table-striped table-bordered table-hover">
                <thead>
    <tr>
    <td>Select</td><td>Field</td><td>Number</td><td>Course</td><td>Term</td>
    </tr>
</thead>
<tbody>
<?

        for($i=0; $i<count($results); $i++)
        {     
        echo '<tr><td><input type="checkbox" class="form" name="checkbox[]" value="'. $results[$i]['cat_num'] . '"</td><td>';
        print_r($results[$i]["field"] . " ");
        echo('</td><td>');
        print_r($results[$i]["number"] . " ");
        echo('</td><td>');
        print_r($results[$i]["title"] . " ");
        echo('</td><td>');
        print_r($results[$i]["term"] . " ");
        echo('</td></tr>');
        }
    }
    ?>
</tbody>
</table>
</fieldset>
</form>
</div>
 <link href="../css/bootstrap.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="../js/holder.js"></script>
</body>
</html>
