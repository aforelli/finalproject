<html>
<body style>
<div class="container">
<div id="middle">


    <!-- Fixed navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php"><img src="img/circle2.png" alt="CS50 Study Buddy" />  CS50 Study Buddy</a>
        </div><div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="dropdown active">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Planner <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="classes.php">Add Courses</a></li>
                  <li><a href="schedule.php">Add Available Times</a></li>
                  <li><a href="appointment.php">Add Appointments</a></li>
                  <li class="divider"></li>
                  <li class="dropdown-header"></li>
                  <li><a href="myclasses.php">My Classes</a></li>
                  <li><a href="updatetimes.php">My Times</a></li>
                  <li><a href="calendar.php">My Calendar</a></li>
                </ul>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Buddy Up <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="findbuddy.php">Find Matches</a></li>
                  <li class="divider"></li>
                  <li class="dropdown-header"></li>
                  <li><a href="buddies.php">My Buddies</a></li>
                </ul>
              </li>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Account <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="settings.php">Settings</a></li>
                  <li class="divider"></li>
                  <li class="dropdown-header"></li>
                  <li><a href="logout.php">Logout</a></li>
                </ul>
              </li>
      </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    
    <div class="container theme-showcase">
        <div class="row">
          <div class="col-lg-12">
            <div class="page-header">
              <h1 id="tables"><br>Weekly Calendar</h1>
            </div>

       
        <div>
        <?php
        if(!empty($appointments))
        {
        ?>
        <form action="calendar.php" method="post">
        <fieldset>
    <div class="bs-example table-responsive">
              <table class="table table-striped table-bordered table-hover">
                <thead>
                  <tr>
                    <th >Select</th>
                    <th>First</th>
                    <th>Last</th>
                    <th>Year</th>
                    <th>Email</th>
                    <th>Times</th>
                  </tr>
                </thead>
                <tbody>
        <?
        for($i=0; $i<count($buddies); $i++)
        {
            echo '<tr><td><input type="checkbox" class="form" name="checkbox[]" value="'. $buddies[$i][0]['id'] . $times[$i] . '"</td><td>';
            print_r($buddies[$i][0]['first'] . " ");
            echo('</td><td>');
            print_r($buddies[$i][0]['last'] . " ");
            echo('</td><td>');
            print_r($buddies[$i][0]['year'] . " ");
            echo('</td><td>');
            print_r($buddies[$i][0]['email'] . " ");
            echo('</td><td>');
            print_r($times[$i] . " ");
            echo('</td></tr>');
        }?>
                </tbody>
                </table>
                <div class="form-group">
            <button type="submit" class="btn btn-default">Remove Appointment</button>
        </div>
                </fieldset>
                </form>
          <?  }   
       
        ?>
        


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
    <link href="../css/bootstrap.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="../js/holder.js"></script>
  </body>
</html>

