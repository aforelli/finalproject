<html>
<body style>
<div class="container">
<div id="middle">

    <!-- Fixed navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/login.php"><img src="img/circle2.png" alt="CS50 Study Buddy" />  CS50 Study Buddy</a>
        </div><div class="navbar-collapse collapse">

        </div><!--/.nav-collapse -->
      </div>
    </div>
    
    <div class="container theme-showcase">
        <div class="page-header">
           <h1 id="tables"><br>Welcome to</h1>
           <a href="/login.php"><img alt="CS50 Study Buddy" src="/img/studybuddy.gif"/></a>
        </div>
      <div class="">
        <p>CS50 Study Buddy is a website created by students for students. Its purpose is to connect classmates with one another so that they can form study groups.
            Study Buddy does this by linking you with your classmates based on a set of criteria, including availability, house or dorm, and year.
            <br><br>
            To begin, enter your username and password to login below, or click register to sign up for an account today.
            </p>
      </div>
    </div> <!-- /container -->
    
    <div class="container theme-showcase">
        <div class="page-header">
           <h1 id="tables">Login</h1>
        </div>
    </div>
    
<form action="login.php" method="post">
    <fieldset>
        <div class="form-group">
            <input autofocus class="form-control" name="username" placeholder="Username" type="text"/>
        </div>
        <div class="form-group">
            <input class="form-control" name="password" placeholder="Password" type="password"/>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-default">Log In</button>
        </div>
    </fieldset>
</form>
<div>
<p>
    or <strong><a href="register.php">register</a></strong> for an account
    <br>
</p>
</div>
</body>
</html>
