<!DOCTYPE html>
<html>
  <head>
    <title>CS50 Study Buddy</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="grid.css" rel="stylesheet">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  
<body>
    <div id="wrap">
    <a class="sr-only" href="#content">Skip to main content</a>

    <!-- Docs master nav -->
    <header class="navbar navbar-inverse navbar-fixed-top bs-docs-nav" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="../" class="navbar-brand">CS50 Study Buddy</a>
    </div>
    <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
      <ul class="nav navbar-nav">
        <li class="active">

        <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Planner <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Add Courses</a></li>
                  <li><a href="#">Add Appointments</a></li>
                  <li class="divider"></li>
                  <li class="dropdown-header"></li>
                  <li><a href="#">My Calendar</a></li>
                </ul>
              </li>
        <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Buddy Up <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Find Matches</a></li>
                  <li class="divider"></li>
                  <li class="dropdown-header"></li>
                  <li><a href="#">My Buddies</a></li>
                </ul>
              </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Account <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Settings</a></li>
                  <li><a href="#">Change Password</a></li>
                  <li class="divider"></li>
                  <li class="dropdown-header"></li>
                  <li><a href="#">Logout</a></li>
                </ul>
              </li>
      </ul>
    </nav>
  </div>
</header>



      <!-- Begin page content -->
      <div class="container">
        <div class="page-header">
          <h1><br>Getting Started</h1>
        </div>
        <p class="lead">CS50 Study Buddy allows Harvard students to connect with classmates by matching them with study buddies.</p>
        <p>Use <a href="../sticky-footer-navbar">the sticky footer with a fixed navbar</a> if need be, too.</p>
      </div>
      
    <div class="container">
      <div class="row">
        <div class="col-md-4">.col-md-4</div>
        <div class="col-md-4">.col-md-4</div>
        <div class="col-md-4">.col-md-4</div>
      </div>
    </div>

    <div id="footer">
      <div class="container">
        <p class="text-muted credit">Copyright 2013 <a href="http://martinbean.co.uk">Ali Forelli</a>, <a href="http://martinbean.co.uk">Gianina Yumul</a>, and <a href="http://ryanfait.com/sticky-footer/">Jenny Chang</a>.</p>
      </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
