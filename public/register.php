<?php

    // configuration
    require("../includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        // check if username was submitted
        if ($_POST["first"] == NULL || $_POST["last"] == NULL)
        {
            apologize("Please enter your name");
        }
        else if ($_POST["year"] == NULL)
        {
            apologize("Please enter your class year");
        }
        else if ($_POST["email"] == NULL)
        {
            apologize("Please enter your email address");
        }
        // check if confirmation and password match
        else if ($_POST["emailconfirmation"] != $_POST["email"])
        {
            apologize("Make sure your emails match");
        }
        else if ($_POST["username"] == NULL)
        {
            apologize("Please enter a username");
        }
        // check if pasword was submitted
        else if ($_POST["password"] == NULL)
        {
            apologize("Please enter a password");
        }
        // check if confirmation and password match
        else if ($_POST["confirmation"] != $_POST["password"])
        {
            apologize("Make sure your passwords match");
        }
    
        // insert data into users
        if (query("INSERT INTO studyusers (first, last, year, email, username, hash) VALUES(?, ?, ?, ?, ?, ?)", 
            $_POST["first"], $_POST["last"], $_POST["year"], $_POST["email"], $_POST["username"], crypt($_POST["password"])) === false)
        {
            apologize("That username is unavailable. Please choose another one");
        }
        
        // report if username is unavailable
        else
        {
            $rows = query("SELECT LAST_INSERT_ID() AS id");
            $id = $rows[0]["id"];
            $_SESSION["id"] = $id;
            redirect("index.php");
        }
    }
    else
    {
        // else render form
        render("register_form.php", ["title" => "Register"]);
    }

?>
