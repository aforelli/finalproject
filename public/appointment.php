<?php
    // configuration
    require("../includes/config.php");
    $k=0;
    $i=0;
    
    // intialize new arrays
    $mybuds=Array();
    $times=Array();
    unset($times);
    
    // query for user's times
    $times=query("SELECT time FROM times WHERE id=?", $_SESSION['id']);
    
    // query to find user's buddies
    $mybudids=query("SELECT budid FROM buddies WHERE id=?", $_SESSION['id']);
    
    // get buddy info
    foreach($mybudids as $mybudid)
    {
        // find buddy info from user table
        $mybuds[$k]=query("SELECT * FROM studyusers WHERE id=?", $mybudid['budid']);
        
        // find buddy's times
        $budtimes[$k]=query("SELECT time from times WHERE id=?", $mybudid['budid']);
        
        // find where buddy and user's times intersect
        $intersect[$k] = array_map("unserialize", array_intersect(array_map("serialize", $budtimes[$k]),array_map("serialize", $times)));
        $intersect[$k] = array_values($intersect[$k]);
        $k++;
    }
    
    // if appointments are selected
    if (isset($_POST['checkbox'])&& isset($_POST['time'])) 
    {
        // buddy name
        $budids=$_POST['checkbox'];
        
        // time of appointment
        $budtimes=$_POST['time'];
        
        // if submitted
        if (empty($budids)==FALSE && empty($budtimes)==FALSE)
        {
            foreach($budids as $budid)
            {
                // get buddy info
                $mybuddies[$i]=query("SELECT * FROM studyusers WHERE id=?", $budid);
                $i++;
                
                // insert appointments into SQL appointments table
                foreach ($budtimes as $budtime)
                {
                    foreach ($mybuddies as $mybuddy)
                    {
                        query("INSERT INTO appointments (id, budid, time) VALUES (?, ?, ?)", $_SESSION['id'], $budid, $budtime);  
                    }
                }
            }
        }
    }
    if(!empty($mybuds) && !empty($intersect))
    {
        render("appointment_form.php", ["title" => "My Appointments", "mybuds" => $mybuds, "intersect" => $intersect]);
    }
    else
    {
        render("appointment_form.php", ["title" => "My Appointments"]);
    }
?>
