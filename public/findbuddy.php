<?php

    // configuration
    require("../includes/config.php");
    
    // initialize arrays, make sure they are empty
    $buddies=Array();
    unset($buddies);
    $usedid=Array();
    unset($usedid);
    $intersect=Array();
    unset($intersect);
    
    // counters
    $i=0;
    $j=0;
    
    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        // user times
        $times=query("SELECT time FROM times WHERE id=?", $_SESSION['id']);
        
        // make sure class was selected
        if (isset($_POST["class"]))
        {
            $cat_nums=$_POST["class"];
            
            // find available buddies for selected class
            foreach($cat_nums as $cat_num)
            {
                // people in same class
                $ids=query("SELECT id FROM userclasses WHERE cat_num=? and id!=?",$cat_num, $_SESSION['id']);
                foreach ($ids as $id)
                {
                    foreach ($times as $time)
                    {
                        // select people that are in the class and share user times
                        $idtimes=query("SELECT id FROM times WHERE time=? and id=?", $time['time'], $id['id']);
                        
                        // get info on these potential buddies
                        foreach ($idtimes as $idtime)
                        {
                            $buddies[$i]=query("SELECT * FROM studyusers WHERE id=?", $idtime['id']);
                            
                            // only increment if query was successful
                            if($buddies[$i]===FALSE)
                            {
                                continue;
                            }
                            else{
                                $i++;
                            }
                            
                        }
                        
                    }
                }
            }
            
        }
        if (!empty($buddies))
            {
            // make sure array is formatted correctly
            $buddies = array_map("unserialize", array_unique(array_map("serialize", $buddies)));
            $buddies=array_values($buddies);
            
            // find intersect times
            foreach ($buddies as $buddy)
            {
            $budtimes[$j]=query("SELECT time from times WHERE id=?", $buddy[0]['id']);
            $intersect[$j] = array_map("unserialize", array_intersect(array_map("serialize", $budtimes[$j]),array_map("serialize", $times)));
            $intersect[$j] = array_values($intersect[$j]);
            $j++;
            }
            
            render("findbuddy_form.php", ["title" => "Buddies", "ids" => $ids, "buddies" => $buddies, "intersect" => $intersect]); 
            }
            else
            {
            apologize("Sorry, no buddies available :("); 
            }
    }
    else
    {
        render("findbuddy_form.php", ["title" => "Buddies"]); 
    }
