<?php

    // configuration
    require("../includes/config.php");
    
    // coounters and arrays
    $i=0;
    $j=0;
    $k=0;
    $mybuddies=Array();
    $mybuds=Array();
    
    // user times
    $times=query("SELECT time FROM times WHERE id=?", $_SESSION['id']);
    
    // if sumbitted
    if (isset($_POST['checkbox'])) 
    {
        // find ids of buddies to delete
        $deletebuddies=$_POST['checkbox'];
        
        if (empty($deletebuddies)==FALSE)
        {
            // delete those buddies from sql table
            foreach($deletebuddies as $deletebuddy)
            {
                query("DELETE FROM buddies WHERE budid=? AND id=?", $deletebuddy, $_SESSION['id']);
            }
            
        }
        // find buddies from sql table
        $mybudids=query("SELECT budid FROM buddies WHERE id=?", $_SESSION['id']);
        // find time intersections
        foreach($mybudids as $mybudid)
        {
            $mybuds[$k]=query("SELECT * FROM studyusers WHERE id=?", $mybudid['budid']);
            $budtimes[$k]=query("SELECT time from times WHERE id=?", $mybudid['budid']);
            $intersect[$k] = array_map("unserialize", array_intersect(array_map("serialize", $budtimes[$k]),array_map("serialize", $times)));
            $intersect[$k] = array_values($intersect[$k]);
            $k++;
        }
        render("buddies_form.php", ["title" => "My Times", "mybuds" => $mybuds, "intersect" => $intersect]);
    }
    
    else
    {
        render("buddies_form.php", ["title" => "My Buddies"]);
    }
?>
