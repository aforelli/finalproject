<?php

    // configuration
    require("../includes/config.php");

    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        // check if year was submitted
        if ($_POST["year"] != NULL)
        {
            // update year
            $rows = query("UPDATE studyusers SET year = ? WHERE id = ?", $_POST["year"], $_SESSION["id"]);
        }
        
        // make sure emails match
        if ($_POST["emailconfirmation"] != $_POST["email"])
        {
            apologize("Make sure your emails match");
            // update email
            $rows = query("UPDATE studyusers SET email = ? WHERE id = ?", $_POST["email"], $_SESSION["id"]);
        }

        // check if username was submitted
        if ($_POST["username"] != NULL)
        {
            // update year
            $rows = query("UPDATE studyusers SET username = ? WHERE id = ?", $_POST["username"], $_SESSION["id"]);
        }
        
        // make sure passwords matches
        if ($_POST["pwconfirmation"] != $_POST["password"])
        {
            apologize("Make sure your passwords match");
            // update password
            $rows = query("UPDATE studyusers SET hash = ? WHERE id = ?", crypt($_POST["password"]), $_SESSION["id"]);
        }
            
        redirect("index.php");
    }
    else
    {
        // else render form
        render("settings.php", ["title" => "Account Settings"]);
    }

?>


