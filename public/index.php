<?php
    /**
     * Computer Science 50
     * Final Project
     * 
     * Ali Forelli, Jenny Chang, and Gianina Yumul
     * December 8, 2013
     *
     */
    
    // We used pset 7 as a starting point for this project
    
    // configuration
    require("../includes/config.php");
    // lookup users
    $users = query("SELECT * FROM studyusers WHERE id = ?", $_SESSION["id"]);

    // render portfolio
    render("portfolio.php", ["title" => "Portfolio", "users" => $users]);

?>
