<?php

    // configuration
    require("../includes/config.php");
 
    // if submitted
    if (isset($_POST['checkbox'])) 
    {
        $deletetimes=$_POST['checkbox'];
        if (empty($deletetimes)==FALSE)
        {
            foreach($deletetimes as $deletetime)
            {
                // update table
                query("DELETE FROM times WHERE time=? AND id=?", $deletetime, $_SESSION['id']);
            }
        }
        render("times_form.php", ["title" => "My Times"]);
    }
    else
    {
        render("times_form.php", ["title" => "My Times"]);
    }
?>
