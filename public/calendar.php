<?php
    // configuration
    require("../includes/config.php");
    
    // if appointment was selected for removal
    if(isset($_POST['checkbox']))
    {
        $strings=$_POST['checkbox'];
        
        // split input into buddy id and time of appointment
        foreach($strings as $string)
        {
            $q=splitAtUpperCase($string);
            $budid=$q[0];
            $time=$q[1];
            // remove appointment
            query("DELETE FROM appointments WHERE budid=? AND time=?", $budid, $time);
        }
        // get appointments
        $appointments=query("SELECT * FROM appointments WHERE id=?", $_SESSION['id']);
        $times=Array();
        $buddies=Array();
        $i=0;
        
        // find buddy info
        foreach ($appointments as $appointment)
        {
            $times[$i]=$appointment['time'];
            $buddies[$i]=query("SELECT * FROM studyusers WHERE id=?", $appointment['budid']);
            $i++;
        }
        
        // render calendar
        render("calendar_form.php", ["title" => "Calendar", "appointments" => $appointments, "times" => $times, "buddies" => $buddies]);
    }
    
    // else, do same thing without deleting from appointments
    else
    {
        $appointments=query("SELECT * FROM appointments WHERE id=?", $_SESSION['id']);
        $times=Array();
        $buddies=Array();
        $i=0;
        foreach ($appointments as $appointment)
        {
            $times[$i]=$appointment['time'];
            $buddies[$i]=query("SELECT * FROM studyusers WHERE id=?", $appointment['budid']);
            $i++;
        }
       
        // render calendar
        render("calendar_form.php", ["title" => "Calendar", "appointments" => $appointments, "times" => $times, "buddies" => $buddies]);
    }
?>
