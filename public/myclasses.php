<?php

    // configuration
    require("../includes/config.php");
    
    // if classesare selected  
    if (isset($_POST['checkbox'])) 
    {
        // get category number of the selected class
        $myclasses=$_POST['checkbox'];
        
        if (empty($myclasses)==FALSE)
        {
            // insert class into sql table
            foreach($myclasses as $myclass)
            {
                $userclasses=query("INSERT into userclasses (id, cat_num) VALUES (?,?)", $_SESSION['id'], $myclass);
            }
        }
        render("myclasses_form.php", ["title" => "My Classes", "my classes" => $myclasses]);
    }
    else
    {
        render("myclasses_form.php", ["title" => "My Classes"]);
    }
?>
