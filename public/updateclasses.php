<?php

    // configuration
    require("../includes/config.php");
 
    // if form was submitted
    if (isset($_POST['checkbox'])) 
    {
        // classes to delete
        $deleteclasses=$_POST['checkbox'];
        if (empty($deleteclasses)==FALSE)
        {
            // delete classes from sql table
            foreach($deleteclasses as $deleteclass)
            {
                query("DELETE FROM userclasses WHERE cat_num=? AND id=?", $deleteclass, $_SESSION['id']);
            }  
        }
        render("myclasses_form.php", ["title" => "My Classes"]);
    }
    else
    {
        render("myclasses_form.php", ["title" => "My Classes"]);
    }
?>
