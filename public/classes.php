<?php

    // configuration
    require("../includes/config.php");
        $result=Array();
        unset($result);
    // if form was submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        // make sure symbol was submitted
        if (empty($_POST["coursename"]))
        {
            apologize("Wow, that seems like an easy class");
        }
        else
        {
            $q=$_POST["coursename"];
            $words=explode(" ",$q);
            $i=0;
            foreach($words as $word)
            {
                if (strcspn($word, '0123456789') != strlen($word))
                {
                    $result[$i]=query("SELECT * FROM courses WHERE number = ?", $word);
                }
                else
                {
                    $result[$i]=query("SELECT * FROM courses WHERE title LIKE '%{$word}%'");
                }
                $i++;
            }
        }
        if (empty($result)==FALSE)
        {
            $results=Array();
            foreach($result as $result)
            {
                $results[]=$result;  
            }
            $results=$results[0];    
            render("classes_form.php", ["title" => "Classes result", "results" => $results]);
        }

        else
        {
            apologize("Tough luck shmuck");
            render("classes_form.php", ["title" => "Classes"]);
        }
    }

    else
    {
        render("classes_form.php", ["title" => "Classes"]);
    }

?>
