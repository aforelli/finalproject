<?php
    // configuration
    require("../includes/config.php"); 
    
    // intitialize arrays and counters
    $i=0;
    $j=0;
    $k=0;
    $mybuddies=Array();
    $mybuds=Array();
    
    // query for user's times
    $times=query("SELECT time FROM times WHERE id=?", $_SESSION['id']);
    
    // if form was submitted
    if (isset($_POST['checkbox'])) 
    {
        // buddy id
        $budids=$_POST['checkbox'];
        
        if (empty($budids)==FALSE)
        {
            foreach($budids as $budid)
            {
                // query for buddies
                $mybuddies[$i]=query("SELECT * FROM studyusers WHERE id=?", $budid);
                $i++;
                
                // insert into buddies sql table
                foreach ($mybuddies as $mybuddy)
                {
                    query("INSERT INTO buddies (id, budid) VALUES (?, ?)", $_SESSION['id'], $budid);
                    $j++;
                }
            }
            
            // query for buddies
            $mybudids=query("SELECT budid FROM buddies WHERE id=?", $_SESSION['id']);
            
            // find intersecting times
            foreach($mybudids as $mybudid)
            {
                $mybuds[$k]=query("SELECT * FROM studyusers WHERE id=?", $mybudid['budid']);
                $budtimes[$k]=query("SELECT time from times WHERE id=?", $mybudid['budid']);
                $intersect[$k] = array_map("unserialize", array_intersect(array_map("serialize", $budtimes[$k]),array_map("serialize", $times)));
                $intersect[$k] = array_values($intersect[$k]);
                $k++;
            }
        }   
        render("buddies_form.php", ["title" => "My Times", "mybuds" => $mybuds, "budids" => $budids, "intersect" => $intersect]);
    }
    
    // else just render form with the below info
    else
    {
        $mybudids=query("SELECT budid FROM buddies WHERE id=?", $_SESSION['id']);
        foreach($mybudids as $mybudid)
        {
            $mybuds[$k]=query("SELECT * FROM studyusers WHERE id=?", $mybudid['budid']);
            $budtimes[$k]=query("SELECT time from times WHERE id=?", $mybudid['budid']);
            $intersect[$k] = array_map("unserialize", array_intersect(array_map("serialize", $budtimes[$k]),array_map("serialize", $times)));
            $intersect[$k] = array_values($intersect[$k]);
            $k++;
        }
        render("buddies_form.php", ["title" => "My Times", "mybuds" => $mybuds, "intersect" => $intersect]);
    }
?>
