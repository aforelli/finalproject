<?php
    require("../includes/config.php"); 
    // get faculty data
    $url2="http://api.cs50.net/courses/2/faculty?key=" . KEY . "&output=php";
    $fac_result_serial = file_get_contents("http://api.cs50.net/courses/2/faculty?key=nl1G2yO3lr4gS89Khy7976mLKyASB9bc&output=php");
    $faculty = unserialize($fac_result_serial);
    foreach ($faculty as $faculty)
    {
        query("INSERT INTO faculty (id, first, middle, last, suffix) VALUES(?,?,?,?,?)", $faculty['id'], $faculty['first'], $faculty['middle'], $faculty['last'], $faculty['suffix']);
    }
    // get field data
    $url3="http://api.cs50.net/courses/2/fields?key=" . KEY . "&output=php";
    $field_result_serial = file_get_contents($url3);
    $fields = unserialize($field_result_serial);
    foreach ($fields as $field)
    {
        query("INSERT INTO fields (id, name) VALUES(?,?)", $field['id'], $field['name']);
        
    }
?>
