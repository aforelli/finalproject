<?php
 // configuration
    require("../includes/config.php"); 
    // get courses data
    $url="http://api.cs50.net/courses/2/courses?key=" . KEY . "&output=php";
    $result_serial = file_get_contents($url);
    $courses = unserialize($result_serial);
    foreach ($courses as $course)
    {
        query("INSERT INTO courses (cat_num, term, bracketed, field, number, title, faculty, description, prerequisites, notes, meetings, building, room) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)", $course['cat_num'], $course['term'], $course['bracketed'], $course['field'], $course['number'], $course['title'], $course['faculty'], $course['description'], $course['prerequisites'], $course['notes'], $course['meetings'], $course['building'], $course['room']);
    }
    
?>
