<?php
    // configure
    require("../includes/config.php"); 
    
    // if submitted
    if (isset($_POST['time'])) 
    {
        // find times
        $times=$_POST['time'];
        if (empty($times)==FALSE)
        {
            // insert into times SQL table
            foreach($times as $time)
            {
                $query = query("INSERT INTO times (id, time) VALUES(?,?)", $_SESSION['id'], $time);
                
            }
            
        }
        render("times_form.php", ["title" => "My Times", "times" => $times]);
    }
    
    else
    {
        render("schedule_form.php", ["title" => "Schedule"]);
    }
?>
