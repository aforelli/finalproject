# README #

Study Buddy is a website created by students for students. Its purpose is to connect classmates with one another so that they can form study groups. Study Buddy does this by linking you with your classmates based on a set of criteria, including availability, house or dorm, and year. The website is ideal for large lecture classes or incoming freshmen, where your ability to work and learn from others is crucial to success but may be hard to cultivate at first. It gives all students equal access to the most important resources on campus - each other.

For a more detailed overview of the project, please refer to the documentation.pdf in the repository. To see the website in action, here's a screencast video: https://www.youtube.com/watch?v=JklZv65l7WQ